#!/bin/bash

echo 'script.sh'

url_arr=("https://gitlab.com/tests-memoire-m2/tests-stage/test1.git
" "https://gitlab.com/tests-memoire-m2/tests-stage/test2.git
")

for path in "${url_arr[@]}"; do
  echo 'path : '
  echo $path
  #cd $path
  git push -u $path -o merge_request.create -o merge_request.target=main
done

